<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function printDigitValue($string = "")
    {
        // Buatlah fungsi untuk menampilkan jumlah digit angka
        //tergantung dimana posisi atau tempat dari angka
        //dalam sebuah string “9.86-A5.321”! Contoh: printDigitValue(‘9.86-A5.321’);
        // Hasil :
        // 9865321
        // 9000000
        // 800000
        // 60000
        // 5000
        // 300
        // 20
        $res = preg_replace("/[^0-9]/", "", $string);
        echo $res . "<br>";
        for ($i = 0; $i < strlen($res); $i++) {
            echo $res[$i] * str_pad('1', strlen($res) - $i, "0") . "<br>";
        }
    }

    public function swap($a, $b)
    {
        // Ketik kode untuk swap 2 integer variables tanpa VARIABLE TAMBAHAN
        //ataupun membuat function dan dalam 3 lines of code (hanya boleh 3 baris):
        // Contoh : let a = 3, let b = 7 => hasilnya a = 7, b = 3
        $a = $a + $b;
        $b = $a - $b;
        $a = $a - $b;

        echo $a;
        echo "<br>";
        echo $b;
    }

    public function sort($a = [], $b = [])
    {
        // Buatlah function yang bisa mengerjakan hal berikut:
        // Tulis function yang bisa menyatukan 2 sorted array menjadi 1 sorted array.
        // Tes apakah jalan dengan input array yang kosong? Atau
        //ada hal lain yang penting untuk dites?
        // E.g. A[] = [1,2,5,7] , B[] = [3,4,8] => C[] = [1,2,3,4,5,7,8]
        // NB : Tidak boleh memakai function seperti sort(), array_merge()
        //atau pakai library apapun. Ingat, inputnya sudah ke sort
        //jadi pasti ada cara yang lebih efisien karena sort() sangat mengambil memory dan waktu!

        $all_arrays = [$a, $b];
        $merged = [];
        for ($i = 0; $i < 2; $i++) {
            $arr = $all_arrays[$i];
            $x = count($arr);
            for ($j = 0; $j < $x; $j++) {
                $merged[$arr[$j]] = 1;
            }
        }
        $final = [];
        $x = count($merged);
        for ($i = 0; $i < $x; $i++) {
            $final[] = key($merged);
            next($merged);
        }
        for ($j = 0; $j < count($final); $j++) {
            for ($i = 0; $i < count($final) - 1; $i++) {

                if ($final[$i] > $final[$i + 1]) {
                    $temp = $final[$i + 1];
                    $final[$i + 1] = $final[$i];
                    $final[$i] = $temp;
                }
            }
        }
        print_r($final);
    }

    //nomor 4
    //Ada 25 kuda dengan berkecapatan berbeda, anda hanya bisa
    //melihat perbedaan kecepatan kuda dengan mengadakan balapan kuda
    //dan hanya bisa mengikut sertakan 5 kuda dalam 1 balapan.
    //Anda tidak punya penghitung waktu, tapi anda bisa mengetahui dari mata Anda
    //dalam 1 balapan siapa yang juara 1 sampai 5. Berapa banyak balapan yang anda
    //harus gelarkan untuk mencari 3 kuda tercepat? NB : kalau anda mentok
    //jawabannya adalah 3+4 hehehe, tapi berikan penjelasannya

    //jwb:

    //anggap aja nama kuda nya yaitu :
    // abcde fghij klmno pqrst uvwxy
    // cari juara 1 tiap perlombaan
    // berarti ada 5 kali perlombaan
    // setalah dpt juara 1 tiap perlombaan
    //berarti ada 5 kuda tersisa, misal kuda afkpu
    //lalu di perlombaan ke 6, di eliminasi kuda terakhir
    // dan tersisa 4 kuda yaitu misal afkp
    //dan di perlombaan ke 7, mencari juara 1 2 3
    // dan otomatis nomor 4 tereliminasi

    //jadi cara nya dgn 7 perlombaan sperti diatas.
}
