<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DigitValueController extends Controller
{
    public function index()
    {
        return $this->printDigitValue('9.86-A5.321');
    }
}
