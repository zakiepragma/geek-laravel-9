<?php

use App\Http\Controllers\DigitValueController;
use App\Http\Controllers\SortController;
use App\Http\Controllers\SwapController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/nomor1', [DigitValueController::class, 'index']);
Route::get('/nomor2', [SwapController::class, 'index']);
Route::get('/nomor3', [SortController::class, 'index']);

// soal : https://docs.google.com/document/d/15FcSa-E3sW5KEYl7ajgdEakIdJACKssD/edit